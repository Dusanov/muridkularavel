{{-- setelah membuat Route dan memberikan data ke view, sebaiknya kita membuat master tamplate seperti dibawah ini,
	untuk mengatur layout dari aplikasi website yang kita buat. 
	setelah kita tambahkan kode bootstrap, selanjtnya kita buat css. --}}

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,
		initial-scale=1">

		<title>Muridku</title>
		<link href="{{ asset
		('bootstrap_3_3_6/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	</head>
	{{-- ini master template, setelah itu kita membuat child-page, 
		pada file homepage.blade.php dan file about.blade.php--}}
	<body>
		<div class="container">
			{{-- kode @include digunakan untuk memasukan file navbar.blade.php --}}
			@include('navbar')
			@yield('main')
		</div>
		@yield('footer')
	
	{{-- memanggil jquery dan bootstrap --}}
	<script src="{{ asset('js/jquery_2_2_1.min.js') }}"></script>
	<script src="{{ asset('bootstrap_3_3_6/js') }}"></script>
	<script src="{{ asset('js/muridkuapp.js') }}">	
	</body>
</html>