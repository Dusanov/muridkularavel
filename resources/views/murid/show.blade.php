@extends('template')

@section('main')
	<div id="murid">
		<h2>Detail Murid</h2>

		<table class="table table-striped">
			<tr>
				<th>NISN</th>
				<td>{{ $murid->nisn }}</td>
			</tr>

			<tr>
				<th>Nama</th>
				<td>{{ $murid->nama_murid }}</td>
			</tr>

			<tr>
				<th>Tanggal Lahir</th>
				<td>{{ $murid->tanggal_lahir->format('d-m-y') }}</td>
			</tr>

			<tr>
				<th>Kelas</th>
				<td>{{ $murid->kelas->nama_kelas }}</td>
			</tr>

			<tr>
				<th>Jenis Kelamin</th>
				<td>{{ $murid->jenis_kelamin }}</td>
			</tr>

			<tr>
				<th>Telepon</th>
				<td>{{ !empty($murid->telepon->nomer_telepon) ? $murid->telepon->nomer_telepon : '-'}}</td>
			</tr>

			<tr>
				<th>Hobi</th>
				<td>
					@foreach($murid->hobi as $item)
						<span>{{ $item->nama_hobi }}</span>,
					@endforeach
				</td>
			</tr>

			<tr>
				<th>Foto</th>
				<td>
					@if(isset($murid->foto))
						<img src="{{ asset('fotoupload/' . $murid->foto) }}">
					@else
						@if ($murid->jenis_kelamin =='L')
						<img src="{{ asset('fotoupload/dummymale.jpg') }}">
						@else
						<img src="{{ asset('fotoupload/dummyfemale.jpg') }}">
						@endif
					@endif
				</td>
			</tr>	
		</table>
	</div>
@stop

@section('footer')
	@include('footer')
@stop
