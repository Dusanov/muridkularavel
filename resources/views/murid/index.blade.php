{{-- file index.blade.php ini di buat setelah kita membuat child-page, 
	file index ini berfungsi untuk menampilkan data, 
	dalam hal ini adalah data murid, selanjutnya kita kembali ke file template.blade.php,
	untuk membuat kode bootstrap. --}}

{{-- pada @extends('template') file ini memanfaatkan template.blade.php sebagai parent-page. --}}
@extends('template')

@section('main')
	<div id="murid">
		<h2>Murid</h2>

		@include('_partial.flash_message')

		@include('murid.form_pencarian')

		 @if (!empty($murid_list))
			<table class="table">
				<thead>
					<tr>
						<th>NISN</th>
						<th>Nama</th>
						<th>Tgl Lahir</th>
						<th>Kelas</th>
						<th>JK</th>
						<th>Telepon</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				 <?php foreach($murid_list as $murid): ?>
				 <tr>
				 	<td>{{ $murid->nisn }}</td>
				 	<td>{{ $murid->nama_murid }}</td>
				 	<td>{{ $murid->tanggal_lahir->format('d-m-y') }}</td>
				 	<td>{{ $murid->kelas->nama_kelas }}</td>
				 	<td>{{ $murid->jenis_kelamin }}</td>
				 	<td>{{ !empty($murid->telepon->nomer_telepon) ? 
				 		$murid->telepon->nomer_telepon : '-'}}</td>
				 	<td>
				 		<div class="box-button">
				 			{{ link_to('murid/' . $murid->id, 'Detail', ['class' => 'btn btn-success btn-sm']) }}
				 		</div>
				 		
				 		<div class="box-button">
				 			{{ link_to('murid/' . $murid->id . '/edit', 'Edit', ['class' => 
				 			'btn btn-warning btn-sm']) }}
				 		</div>

				 		<div class="box-button">
				 			{!! Form::open(['method' => 'DELETE', 'action' => ['SiswaController@destroy', 
				 			$murid->id]]) !!}
				 			{!! Form::submit('DELETE', ['class' => 'btn btn-danger btn-sm']) !!}
				 			{!! Form::close() !!}
				 		</div>
				 	</td>
				 </tr>
				 <?php endforeach ?>
				</tbody>
			</table>
		 @else
			<p>Tidak ada data murid.</p>
		 @endif

		 <div class="table-nav">
		 	<div class="jumlah-data">
		 		<strong>Jumlah Murid : {{ $jumlah_murid }}</strong>
		 	</div>
		 	<div class="paging">
		 		{{ $murid_list->links() }}
		 	</div>
		 </div>

		 <div class="tombol-nav">
		 	<div>
		 		<a href="{{ url('murid/create') }}" class="btn btn-primary">Tambah Murid</a>
		 	</div>
		 </div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop
				
			