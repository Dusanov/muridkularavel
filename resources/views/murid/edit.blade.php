@extends('template')

@section('main')
	<div id="murid">
		<h2>Edit Murid</h2>

		{!! Form::model($murid, ['method' => 'PATCH', 'files' => true, 'action' => 
		['SiswaController@update', $murid->id]]) !!}
		@include('murid.form', ['submitButtonText' => 'Update'])		
		{!! Form::close() !!}
	</div>
@stop
@section('footer')
	@include('footer')
@stop		