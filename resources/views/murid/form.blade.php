@if (isset($murid))
	{!! Form::hidden('id', $murid->id) !!}
@endif

@if ($errors->any())
	<div class="form-group {{ $errors->has('nisn') ? 'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
	{!! Form::label('nisn', 'NISN:', ['class' => 'control-label']) !!}
	{!! Form::text('nisn', null, ['class' => 'form-control']) !!}
			@if ($errors->any('nisn'))
			<span class="help-block">{{ $errors->first('nisn') }}</span>
			@endif
	</div>


@if ($errors->any())
	<div class="form-group {{ $errors->has('nama_murid') ? 'has-error' : 'has-success'}}">
@else
	<div class="form-group">
@endif
	{!! Form::label('nama_murid', 'Nama:', ['class' => 'control-label']) !!}
	{!! Form::text('nama_murid', null, ['class' => 'form-control']) !!}
			@if ($errors->has('nama_murid'))
			<span class="help-block">{{ $errors->first('nama_murid') }}</span>
			@endif
	</div>


@if ($errors->any())
	<div class="form-group {{ $errors->has('tanggal_lahir') ? 'has-error' : 'has-success'}}">
@else
	<div class="form-group">
@endif
	{!! Form::label('tanggal_lahir', 'Tanggal Lahir:', ['class' => 'control-label']) !!}
	{!! Form::date('tanggal_lahir', !empty($murid) ? $murid->tanggal_lahir->format('Y-m-d'): null, 
					['class' => 'form-control', 'id' => 'tanggal_lahir']) !!}
			@if($errors->has('tanggal_lahir'))
			<span class="help-block">{{ $errors->first('tanggal_lahir') }}</span>
			@endif
	</div>


@if ($errors->any())
	<div class="form-group {{ $errors->has('id_kelas') ?
	'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
	{!! Form::label('id_kelas', 'Kelas:', ['class' => 'control-label']) !!}
			@if(count($list_kelas) > 0)
				{!! Form::select('id_kelas', $list_kelas, null, ['class' => 'form-control', 'id'
				 => 'id_kelas', 'placeholder' => 'Pilih Kelas']) !!}
			@else
				<p>Tidak ada pilihan kelas, saya buat dulu deh...!</p>
			@endif
			@if ($errors->has('id_kelas'))
			<span class="help-block">{{ $errors->first('id_kelas') }}</span>
			@endif
	</div>


@if ($errors->any())
	<div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : 'has-success'}}">
@else
	<div class="form-group">
@endif
	{!! Form::label('jenis_kelamin', 'Jenis Kelamin:', ['class' => 'control-label']) !!}
		<div class="radio">
			<label>{!! Form::radio('jenis_kelamin', 'L') !!}Laki-laki</label>
		</div>

		<div class="radio">
			<label>{!! Form::radio('jenis_kelamin', 'P') !!}Perempuan</label>
		</div>
		@if($errors->has('jenis_kelamin'))
		<span class="help-block">{{ $errors->first('jenis_kelamin') }}</span>
		@endif
	</div>


@if($errors->any())
	<div class="form-group {{ $errors->has('nomer_telepon') ? 'has-error' : 'has-success'}}">
@else
	<div class="form-group">
@endif
	{!! Form::label('nomer_telepon', 'Telepon:', ['class' => 'control-label']) !!}
	{!! Form::text('nomer_telepon', null, ['class'=>'form-control']) !!}
			@if($errors->has('nomer_telepon'))
			<span class="help-block">{{ $errors->first('nomer_telepon') }}</span>
			@endif
	</div>


@if($errors->any())
	<div class="form-group {{ $errors->has('hobi') ? 'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
	{!! Form::label('hobi_murid', 'Hoby:', ['class' => 'control-label']) !!}
			@if(count($list_hobi) > 0)
				@foreach($list_hobi as $key => $value)
					<div class="checkbox">
					<label>{!! Form::checkbox('hobi_murid[]', $key, null) !!}
					{{ $value }}</label>
					</div>
				@endforeach
			@else
				<p>Tidak ada pilihan hobi, buat dulu ya...!</p>
			@endif
	</div>

{{-- input file untuk foto--}}
@if($errors->any())
	<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
	{!! Form::label('foto', 'Foto:') !!}
	{!! Form::file('foto') !!}
	@if ($errors->has('foto'))
	<span class="help-block">{{ $errors->first('foto') }}</span>
	@endif
	</div>
	@if(isset($murid))	
		@if(isset($murid->foto))
			<img src="{{ asset('fotoupload/' . $murid->foto) }}">
		@else
			@if ($murid->jenis_kelamin =='L')
			<img src="{{ asset('fotoupload/dummymale.jpg') }}">
			@else
			<img src="{{ asset('fotoupload/dummyfemale.jpg') }}">
			@endif
		@endif
	@endif
	
<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>