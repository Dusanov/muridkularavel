@extends('template')

@section('main')
	<div id="murid">
		<h2>Tambah Murid</h2>
		{{--@include('errors.form_error_list')--}}
		{!! Form::open(['url' => 'murid', 'files' => 'true']) !!}
		@include('murid.form', ['submitButtonText' => 'Simpan'])	
		{!! Form::close() !!}
	</div>
@stop

@section('footer')
	@include('footer')
@stop	 