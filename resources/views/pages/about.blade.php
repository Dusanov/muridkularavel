{{-- setelah membuat route didalam folder routes,
	setelah itu kita membuat file about di dalam folder pages/views. 
	dan file about.blade.php ini berfungsi sebagai child-page. --}}

@extends('template')

@section('main')
	<div id="about">
		<h2>About</h2>
		<p>Website <strong>laravelapp</strong> dibuat sebagai
		latihan untuk mempelajari laravel.</p>
	</div>
@stop