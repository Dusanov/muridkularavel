{{-- setelah membuat route didalam folder routes,
	setelah itu kita membuat file homepage ini di dalam folder pages/views. --}}
	
{{-- @extends, @section berfungsi sebagai child-page. setelah membuat child-page, selanjutnya kita buat file index --}}
@extends('template')

@section('main')
	{{-- <div id="homepage" ini adalah file homepage yang dibuat setelah route homepage, 
	selanjutnya kita kembali ke route untuk membuat daftar murid. --}}
	<div id="homepage">
		<h2>Homepage</h2>
		<p>Selamat Belajar Laravel!</p>
	</div>
@stop

@section('footer')
	@include('footer')
@stop