<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHobiMurid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hobi_murid', function (Blueprint $table) {
            $table->integer('id_murid')->unsigned()->index();
            $table->integer('id_hobi')->unsigned()->index();
            $table->timestamps();

            $table->primary(['id_murid', 'id_hobi']);

            $table->foreign('id_murid')
                  ->references('id')
                  ->on('murid')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('id_hobi')
                  ->references('id')
                  ->on('hobi')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hobi_murid');
    }
}
