<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//buat Route terlebih dahulu, setelah itu membuat views, dengan cara, 
//buat folder pages di dalam folder views, yang berisikan, homepage dan about.
Route::get('/', 'PagesController@homepage');

Route::get('murid/cari', 'SiswaController@cari');

Route::resource('murid', 'SiswaController');

Route::get('about', 'PagesController@about');

Route::get('halaman-rahasia', 'RahasiaController@halamanRahasia')->name('secret');

Route::get('showmesecret', 'RahasiaController@showMeSecret');

Route::get('murid', 'SiswaController@index');

Route::get('murid/create', 'SiswaController@create');

Route::get('murid/{murid}', 'SiswaController@show');

Route::post('murid', 'SiswaController@store');

Route::get('murid/{murid}/edit', 'SiswaController@edit');

Route::patch('murid/{murid}', 'SiswaController@update');

Route::delete('murid/{murid}', 'SiswaController@destroy');

Route::get('tes-collection', 'SiswaController@tesCollection');

Route::get('date-mutator', 'SiswaController@dateMutator');

//untuk membuat daftar murid, sementara kita membuatnya di dalam Route sampledata.
//setelah membuat daftar murid ini, selanjutnya kita membuat master template.
//lanjut ke folder pages.
Route::get('sampledata', function() {
	DB::table('murid')->insert([
		[
			'nisn'			=> '1001',
			'nama_murid'	=> 'Agus Yulianto',
			'tanggal_lahir'	=> '1990-02-12',
			'jenis_kelamin'	=> 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1002',
			'nama_murid'	=> 'Agustina Anggraeni',
			'tanggal_lahir'	=> '1990-03-01',
			'jenis_kelamin'	=> 'P',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1003',
			'nama_murid'	=> 'Bayu Firmansyah',
			'tanggal_lahir'	=> '1990-06-17',
			'jenis_kelamin' => 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1004',
			'nama_murid'	=> 'Citra Rahmawati',
			'tanggal_lahir'	=> '1991-12-12',
			'jenis_kelamin' => 'P',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1005',
			'nama_murid'	=> 'Dirgantara Laksana',
			'tanggal_lahir' => '1990-10-10',
			'jenis_kelamin' => 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1006',
			'nama_murid'	=> 'Eko Satrio',
			'tanggal_lahir' => '1990-07-14',
			'jenis_kelamin' => 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1007',
			'nama_murid'	=> 'Firda Ayu Larasati',
			'tanggal_lahir' => '1992-02-02',
			'jenis_kelamin' => 'P',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1008',
			'nama_murid'	=> 'Galang Rambu Anarki',
			'tanggal_lahir' => '1991-05-11',
			'jenis_kelamin' => 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1009',
			'nama_murid'	=> 'Haris Purnomo',
			'tanggal_lahir' => '1991-10-10',
			'jenis_kelamin' => 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
		[
			'nisn'			=> '1010',
			'nama_murid'	=> 'Indra Birowo',
			'tanggal_lahir' => '1991-12-04',
			'jenis_kelamin' => 'L',
			'created_at'	=> '2016-03-10 19:10:15',
			'updated_at'	=> '2016-03-10 19:10:15'
		],
	]);
});