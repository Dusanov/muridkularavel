<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hoby extends Model
{
    protected $table = 'hobi';

    protected $fillable = ['nama_hobi'];

    public function murid() {
    	return $this->belongsToMany('App\Murid', 'hobi_murid', 'id_hobi', 'id_murid');
    }
}
