<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class MuridkuAppServiceProvider extends ServiceProvider
{
    
    public function register()
    {
        //
    }

   
    public function boot()
    {
        $halaman = '';
        if (Request::segment(1) == 'murid') {
            $halaman = 'murid';
        }

        if (Request::segment(1) == 'about') {
            $halaman = 'about';
        }
        view()->share('halaman', $halaman);
    }
}
