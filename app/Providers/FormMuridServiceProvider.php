<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Kelas;
use App\Hoby;

class FormMuridServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //view()->composer('murid.form', function($view) {
        //$view->with('list_kelas', Kelas::lists('nama_kelas', 'id'));
        //$view->with('list_hobi', Hoby::lists('nama_hobi', 'id'));
        //});

        //view()->composer('murid.form_pencarian', function($view) {
            //$view->with('list_kelas', Kelas::lists('nama_kelas', 'id'));
        //});
    }
}
