<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Murid;
//use Validator;
use App\Telepon;
use App\Kelas;
use App\Hoby;
//use App\Http\Requests\MuridRequest;
use Storage;
use Session;

class SiswaController extends Controller
{
    public function index() {
    	$murid_list    = Murid::orderBy('nama_murid', 'asc')->paginate(5);
    	$jumlah_murid  = Murid::count();
        return view('murid.index', compact('murid_list', 'jumlah_murid'));
    }

    
    public function create() {
        $list_kelas = Kelas::pluck('nama_kelas', 'id');
        $list_hobi  = Hoby::pluck('nama_hobi', 'id');
        return view('murid.create', compact('list_kelas', 'list_hobi'));
    }

    
    public function store(Request $request) {
        // $murid = new Murid;
        // $murid->nisn = $request->nisn;
        // $murid->nama_murid = $request->nama_murid;
        // $murid->tanggal_lahir = $request->tanggal_lahir;
        // $murid->jenis_kelamin = $request->jenis_kelamin;
        // $murid->save();

        //Murid::create($request->all());
        
        $input = $request->all();
        //$validator = Validator::make($input, [
        $this->validate($request, [
            'nisn' => 'required|string|size:4|unique:murid,nisn',
            'nama_murid' => 'required|string|max:30',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|in:L,P',
            'nomer_telepon' => 'sometimes|nullable|numeric|digits_between:10,15|
             unique:telepon,nomer_telepon',
            'id_kelas'      => 'required',
        ]);

        //if ($validator->fails()) {
            //return redirect('murid/create')->withInput()->withErrors($validator);
        //}
        if($request->hasFile('foto')) {
           $foto = $request->file('foto');
           $ext  = $foto->getClientOriginalExtension();
           if ($request->file('foto')->isValid()) {
             $foto_name = date('YmdHis'). ".$ext";
             $upload_path = 'fotoupload';
             $request->file('foto')->move($upload_path, $foto_name);
             $input['foto'] = $foto_name;
           } 
        }

        $murid = Murid::create($input);

        //if ($request->filled('nomer_telepon')) {

        $telepon = new Telepon;
        $telepon->nomer_telepon = $request->input('nomer_telepon');
        $murid->telepon()->save($telepon);

        //}

        $murid->hobi()->attach($request->input('hobi_murid'));

        Session::flash('flash_message', 'Data murid berhasil disimpan.');

        return redirect ('murid');
    }

    
    
    public function show(Murid $murid) {
        //$murid   =  Murid::findOrFail($id);
        return view('murid.show', compact('murid'));
    }

    
    public function edit(Murid $murid) {
        //$murid = Murid::findOrFail($id);
        $list_kelas = Kelas::pluck('nama_kelas', 'id');
        $list_hobi = Hoby::pluck('nama_hobi', 'id');
        
        if(!empty($murid->telepon->nomer_telepon)) {
        $murid->nomer_telepon = $murid->telepon->nomer_telepon;
        }
        
        return view('murid.edit', compact('murid', 'list_kelas', 'list_hobi'));
    }

    
    public function update(Murid $murid, Request $request) {
        //$murid = Murid::findOrFail($id);
        $input = $request->all();

        if ($request->hasfile('foto')) {
            $exist = Storage::disk('foto')->exists($murid->foto);
            if(isset($murid->foto) && $exist) {
                $delete = Storage::disk('foto')->delete($murid->foto);
            }

            $foto = $request->file('foto');
            $ext  = $foto->getClientOriginalExtension();
            if ($request->file('foto')->isValid()) {
                $foto_name      = date('YmdHis'). ".$ext";
                $upload_path    = 'fotoupload';
                $request->file('foto')->move($upload_path, $foto_name);
                $input['foto'] = $foto_name;
            }
        }



        //$validator = Validator::make($input, [
        //$this->validate($request, [
            //'nisn' => 'required|string|size:4|unique:murid,nisn,' . $request->input('id'),
            //'nama_murid' => 'required|string|max:30',
            //'tanggal_lahir' => 'required|date',
            //'jenis_kelamin' => 'required|in:L,P',
        //]);

        //if ($validator->fails()) {
            //return redirect('murid/' . $id . '/edit')->withInput()->withErrors($validator);
        //}
        
        $murid->update($input);

        //Update nomor telepon, jika sebelumnya sudah ada no telepon.

        if($murid->telepon) {
            if ($request->filled('nomer_telepon')) {
                $telepon = $murid->telepon;
                $telepon->nomer_telepon = $request->input('nomer_telepon');
                $murid->telepon()->save($telepon);
            }

            // Jika telepon tidak di isi, hapus.

            else{
                $murid->telepon()->delete();
            }
        }

        // Buat entry baru, jika sebelumnya tidak ada no telepon.

        else{
            if($request->filled('nomer_telepon')) {
                $telepon = new Telepon;
                $telepon->nomer_telepon = $request->input('nomer_telepon');
                $murid->telepon()->save($telepon);
            }
        }

        $murid->hobi()->sync($request->input('hobi_murid'));

        Session::flash('flash_message', 'Data murid berhasil di update.');
        
        return redirect('murid');
    }

    
    public function destroy(Murid $murid) {
        //$murid = Murid::findOrFail($id);
        //$murid->delete();
        $exist = Storage::disk('foto')->exists($murid->foto);
        if (isset($murid->foto) && $exist) {
            $delete = Storage::disk('foto')->delete($murid->foto);
        }

        $murid->delete();
        Session::flash('flash_message', 'Data murid berhasil dihapus.');
        Session::flash('penting', true);
        return redirect('murid');
    }

    
    public function tesCollection() {
        $data = [
            ['nisn' => '1001', 'nama_murid' => 'Agus Yulianto'],
            ['nisn' => '1002', 'nama_murid' => 'Agustina Anggraeni'],
            ['nisn' => '1003', 'nama_murid' => 'Bayu Firmansyah'],
        ];
        $collection = collect($data);
        $collection->toJson();
        return $collection;
        }

    
    public function dateMutator() {
        $murid = Murid::findOrFail(1);
        //dd($murid->tanggal_lahir);
        //dd($murid->created_at);
        $nama = $murid->nama_murid;
        $tanggal_lahir = $murid->tanggal_lahir->format('d-m-y');
        $ulang_tahun = $murid->tanggal_lahir->addYears(30)->format('d-m-y');
        //return "Umur {$murid->nama_murid} adalah {$murid->tanggal_lahir->age} tahun.";
        return "Murid {$nama} lahir pada {$tanggal_lahir}.
        <br> Ulang tahun ke-30 akan jatuh pada {$ulang_tahun}.";
    }

    public function cari (Request $request) {
        $kata_kunci     =  trim($request->input('kata_kunci'));

        if(!empty($kata_kunci)) {
            $jenis_kelamin = $request->input('jenis_kelamin');
            $id_kelas      = $request->input('id_kelas');
        
            $query          = Murid::where('nama_murid', 'LIKE', '%' . $kata_kunci . '%');
            (! empty($jenis_kelamin)) ? $query ->where('jenis_kelamin', $jenis_kelamin) : '';
            (! empty($id_kelas)) ? $query ->where('id_kelas', $id_kelas) : '';
                $murid_list     = $query->paginate(2);
        
    $pagination     = (! empty($jenis_kelamin)) ? 
    $murid_list->appends(['jenis_kelamin' => $jenis_kelamin]) : '';
    $pagination     = (! empty($id_kelas)) ? $pagination =
    $murid_list->appends(['id_kelas' => $id_kelas]) : '';
    $pagination     = $murid_list->appends(['kata_kunci' => $kata_kunci]);
        
                $jumlah_murid   = $murid_list->total();
                return view('murid.index', compact('murid_list', 
                    'kata_kunci', 'pagination', 'jumlah_murid', 'id_kelas', 'jenis_kelamin'));
        }

        return redirect('murid');
    }

}    