<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MuridRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nisn' => 'required|string|size:4|unique:murid,nisn',
            'nama_murid' => 'required|string|max:30',
            'tanggal_lahir' => 'required|date',
            'jenis_kelamin' => 'required|in:L,P',
            'nomer_telepon' => 'sometimes|nullable|numeric|digits_between:10,15|
                                unique:telepon,nomer_telepon',
            'id_kelas'      => 'required',
            'foto'          => 'sometimes|nullable|image|mimes:jpeg,jpg,png|max:500
                                |dimension:width=150,height=180',
        ];
    }
}

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
