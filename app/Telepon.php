<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telepon extends Model
{
    protected $table = 'telepon';
    protected $primaryKey = 'id_murid';
    protected $fillable = ['id_murid', 'nomer_telepon'];

    //kode inilah yang membentuk relasi antara object Telepon{} dan Murid{}
    public function murid() {
    	return $this->belongsTo('App\Murid', 'id_murid');
    }
}
