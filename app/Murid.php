<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Murid extends Model
{
    protected $table = 'murid';

    protected $fillable = [
    		'nisn',
    		'nama_murid',
    		'tanggal_lahir',
    		'jenis_kelamin',
            'id_kelas',
            'foto',
    ];

    protected $dates = ['tanggal_lahir'];

    public function getNamaMuridAttribute($nama_murid){

    	// fungsi get itu secara "common sense" ada return, ngembaliin nilai
    	// nilainya mau dikasih ke pihak lain
    	return ucwords($nama_murid);
    }


    public function setNamaMuridAttribute($nama_murid) {

    	// fungsi secara "common sense" ga ada return, ga ngembaliin nilai
    	// dia NARO nilai
    	$this->attributes['nama_murid'] =  strtolower($nama_murid);
    }

    public function telepon() {
        return $this->hasOne('App\Telepon', 'id_murid');
    }

    public function kelas() {
        return $this->belongsTo('App\Kelas', 'id_kelas');
    }

    public function hobi() {
        return $this->belongsToMany('App\Hoby', 'hobi_murid', 'id_murid', 'id_hobi')
        ->withTimeStamps();
    }

    public function getHobyMuridAttribute() {
        return $this->hobi->pluck('id')->toArray();
    }


}

